package com.example.springsoapwsexception;

import org.springframework.ws.soap.*;
import org.springframework.ws.soap.server.endpoint.*;
import org.springframework.xml.namespace.*;

import javax.xml.bind.*;
import javax.xml.namespace.*;
import javax.xml.soap.*;
import java.beans.*;
import java.util.*;

public class DetailSoapFaultDefinitionExceptionResolver extends SoapFaultMappingExceptionResolver {

    private static final QName CODE = new QName("code");
    private static final QName DESCRIPTION = new QName("description");

    private static final QName TEST = new QName("test");

    QName bodyName = new QName("http://services.ws.gpp.sinam.net/", "WSException", "ns2");


    @Override
    protected void customizeFault(Object endpoint, Exception ex, SoapFault fault) {
        logger.warn("Exception processed ", ex);
        if (ex instanceof ServiceFaultException) {
            ServiceFault serviceFault = ((ServiceFaultException) ex).getServiceFault();


            SoapFaultDetail detail = fault.addFaultDetail();
            detail.addFaultDetailElement(CODE).addText(serviceFault.getCode());
            detail.addFaultDetailElement(bodyName);
            detail.addFaultDetailElement(DESCRIPTION).addText(serviceFault.getDescription());
            detail.addFaultDetailElement(TEST).addText(serviceFault.getTest());

            SoapFaultDetailElement soapFaultDetailElement = detail.addFaultDetailElement(new QName("sasadf"));

            soapFaultDetailElement.addNamespaceDeclaration("dsfafdsa", "dfsafas");

            TestClass testClass = new TestClass();
            testClass.setTest1("test1");
            testClass.setTest2("test2");
            testClass.setTest3("test3");

            Class<?> clazz = testClass.getClass();

            QName name = new QName(Introspector.decapitalize(clazz.getSimpleName()));
            JAXBElement jaxbElement = new JAXBElement(name, clazz, testClass);
            SoapFaultDetailElement el = detail.addFaultDetailElement(name);
            el.addAttribute(CODE,"Quantity element does not have a value");

            Iterator<SoapFaultDetailElement> ddd = detail.getDetailEntries();
            SoapFaultDetailElement ddds = ddd.next();
        }
    }
}
