package com.example.springsoapwsexception;

public class MissingName extends Exception {
  public MissingName() {
    super("Your name is required.");
  }
}