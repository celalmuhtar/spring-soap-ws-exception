package com.example.springsoapwsexception;

import com.memorynotfound.beer.*;
import org.springframework.ws.server.endpoint.annotation.*;

@Endpoint
public class BeerEndpoint {

    public static final String NAMESPACE_URI = "http://memorynotfound.com/beer";

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getBeerRequest")
    @ResponsePayload
    public GetBeerResponse getBeer(@RequestPayload GetBeerRequest request) {
        throw new ServiceFaultException("CustomError",new ServiceFault(
                "NOT_FOUND", "Beer with id: " + request.getId() + " not found.", "test"));
    }

}
