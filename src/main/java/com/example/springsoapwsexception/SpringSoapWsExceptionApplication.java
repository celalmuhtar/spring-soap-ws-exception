package com.example.springsoapwsexception;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSoapWsExceptionApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringSoapWsExceptionApplication.class, args);
	}

}
